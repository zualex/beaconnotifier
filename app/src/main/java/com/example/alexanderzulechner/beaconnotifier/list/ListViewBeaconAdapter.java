package com.example.alexanderzulechner.beaconnotifier.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import com.estimote.sdk.Beacon;
import com.example.alexanderzulechner.beaconnotifier.MyApplication;
import com.example.alexanderzulechner.beaconnotifier.R;
import com.example.alexanderzulechner.beaconnotifier.model.BeaconModel;

import java.util.ArrayList;

/**
 * Created by alexanderzulechner on 04.01.16.
 */

public class ListViewBeaconAdapter extends BaseAdapter {
    private ArrayList<BeaconModel> list = new ArrayList<BeaconModel>();
    private Context context;

    public ListViewBeaconAdapter(Context context) {
        this.context = context;
        list = new ArrayList<BeaconModel>();
        list.add(new BeaconModel(1, "test", "test", "test", "test", 23.0, 24.0, 34.0));
    }

    @Override
    public int getCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        this.list = ((MyApplication) this.context.getApplicationContext()).getBeaconList();

        TwoLineListItem twoLineListItem;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            twoLineListItem = (TwoLineListItem) inflater.inflate(
                    android.R.layout.simple_list_item_2, null);
        } else {
            twoLineListItem = (TwoLineListItem) convertView;
        }

        TextView text1 = twoLineListItem.getText1();
        TextView text2 = twoLineListItem.getText2();

        if (list != null) {
            text1.setText("Test1");
            text2.setText("Test2");
            /*text1.setText(list.get(position).getShortText());
            text2.setText("" + list.get(position).getTemp());*/
        } else {
            text1.setText("Test1");
            text2.setText("Test2");
        }


        return twoLineListItem;
    }
}
