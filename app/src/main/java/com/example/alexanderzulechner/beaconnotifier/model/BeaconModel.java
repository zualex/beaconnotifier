package com.example.alexanderzulechner.beaconnotifier.model;

/**
 * Created by alexanderzulechner on 01.11.15.
 */
public class BeaconModel {
    private int id;
    private String namespace;
    private String instance;
    private String shortText;
    private String longText;
    private Double temp;
    private Double minTemp;
    private Double maxTemp;

    public BeaconModel(int id, String namespace, String instance, String shortText, String longText, Double temp, Double minTemp, Double maxTemp) {
        this.id = id;
        this.namespace = namespace;
        this.instance = instance;
        this.shortText = shortText;
        this.longText = longText;
        this.temp = temp;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getLongText() {
        return longText;
    }

    public void setLongText(String longText) {
        this.longText = longText;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Double getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Double minTemp) {
        this.minTemp = minTemp;
    }

    public Double getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Double maxTemp) {
        this.maxTemp = maxTemp;
    }

}
