package com.example.alexanderzulechner.beaconnotifier;

import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.alexanderzulechner.beaconnotifier.list.ListViewBeaconAdapter;
import com.example.alexanderzulechner.beaconnotifier.model.BeaconModel;

import java.util.ArrayList;

/**
 * Created by alexanderzulechner on 19.10.15.
 */
public class BeaconListFragment extends ListFragment {

    /**
     * TEST
     * String[] numbers_text = new String[]{"one", "two", "three", "four",
     * "five", "six", "seven", "eight", "nine", "ten", "eleven",
     * "twelve", "thirteen", "fourteen", "fifteen"};
     * String[] numbers_digits = new String[]{"1", "2", "3", "4", "5", "6", "7",
     * "8", "9", "10", "11", "12", "13", "14", "15"};
     *
     * @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
     * Bundle savedInstanceState) {
     * ArrayAdapter<String> adapter = new ArrayAdapter<String>(
     * inflater.getContext(), android.R.layout.simple_list_item_1,
     * numbers_text);
     * setListAdapter(adapter);
     * return super.onCreateView(inflater, container, savedInstanceState);
     * }
     * }
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fraglayout = inflater.inflate(R.layout.beacon_list_layout, container, false);
        this.setListAdapter(new ListViewBeaconAdapter(getActivity()));
        return fraglayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}

