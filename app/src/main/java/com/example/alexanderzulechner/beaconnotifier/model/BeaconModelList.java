package com.example.alexanderzulechner.beaconnotifier.model;

import android.util.Log;

import com.estimote.sdk.eddystone.Eddystone;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by alexanderzulechner on 02.01.16.
 */
public class BeaconModelList {

    private ArrayList<BeaconModel> beaconList = new ArrayList<BeaconModel>();
    private BeaconAPI api = BeaconAPI.getInstance();


    public ArrayList<BeaconModel> getBeaconList() {
        return beaconList;
    }

    public void syncRangeList(List<Eddystone> eddystones) {
        beaconList = new ArrayList<BeaconModel>();
        for (Eddystone e : eddystones) {
            Log.d("SYNCRANGELIST", "Beacon" + e);
            addBeaconFromDBToList(e);
        }
    }

    private void addBeaconFromDBToList(final Eddystone e) {

        // Prepare the HTTP request & asynchronously execute HTTP request
        Call<BeaconAPI.HttpBinResponse> call = api.getService().getBeacon(e.namespace, e.instance);

        call.enqueue(new Callback<BeaconAPI.HttpBinResponse>() {
            /**
             * onResponse is called when any kind of response has been received.
             */
            public void onResponse(Response<BeaconAPI.HttpBinResponse> response, Retrofit retrofit) {
                int id;
                String shortText;
                String longText;
                Double minTemp;
                Double maxTemp;
                Double temp = 0.0;

                if (!response.isSuccess()) {
                    // print response body if unsuccessful
                    try {
                        System.out.println(response.errorBody().string());
                    } catch (IOException e) {
                        // do nothing
                    }
                    return;
                }

                // if parsing the JSON body failed, `response.body()` returns null
                BeaconAPI.HttpBinResponse decodedResponse = response.body();
                if (decodedResponse == null) {
                    return;
                }

                id = decodedResponse.id;
                shortText = decodedResponse.shortText;
                longText = decodedResponse.longText;
                minTemp = decodedResponse.minTemp;
                maxTemp = decodedResponse.maxTemp;
                try {
                    temp = e.telemetry.temperature;
                } catch (Throwable t) {
                    System.out.println("Temperature " + t.getMessage());
                }

                // at this point the JSON body has been successfully parsed
                beaconList.add(new BeaconModel(id, e.namespace, e.instance, shortText, longText, temp, minTemp, maxTemp));
            }

            /**
             * onFailure gets called when the HTTP request didn't get through.
             * For instance if the URL is invalid / host not reachable
             */
            public void onFailure(Throwable t) {
                Log.d("BEACONMODELLIST", "onFAILURE" + t);
            }
        });
    }

/*    private BeaconModel getBeaconListFromDB(Eddystone e) {
        BeaconModel b;

        // Prepare the HTTP request & asynchronously execute HTTP request
        Call<List<BeaconAPI.HttpBinResponse>> call = api.getService().getBeaconList();//(e.namespace, e.instance);
        call.enqueue(new Callback<List<BeaconAPI.HttpBinResponse>>() {

            public void onResponse(Response<List<BeaconAPI.HttpBinResponse>> response, Retrofit retrofit) {
                // http response status code + headers
                System.out.println("Response status code: " + response.code());

                // isSuccess is true if response code => 200 and <= 300
                if (!response.isSuccess()) {
                    // print response body if unsuccessful
                    try {
                        System.out.println(response.errorBody().string());
                    } catch (IOException e) {
                        // do nothing
                    }
                    return;
                }

                // if parsing the JSON body failed, `response.body()` returns null
                List<BeaconAPI.HttpBinResponse> decodedResponse = response.body();
                if (decodedResponse == null) {
                    return;
                }

                // at this point the JSON body has been successfully parsed
                for (BeaconAPI.HttpBinResponse b : decodedResponse) {
                    System.out.println("response: " + b.shortText);
                }

            }
            public void onFailure(Throwable t) {
                Log.d("BEACONMODELLIST", "onFAILURE" + t);
            }
        });

        b = new BeaconModel(1, e.namespace, e.instance, "Küchenmaschinen apparat", "Küche", 23.4, 25.0, 26.0);
        return b;
    }
        */
}