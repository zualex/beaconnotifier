package com.example.alexanderzulechner.beaconnotifier;

/**
 * Created by alexanderzulechner on 05.10.15.
 */

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.EstimoteSDK;
import com.estimote.sdk.eddystone.Eddystone;
import com.example.alexanderzulechner.beaconnotifier.model.BeaconModel;
import com.example.alexanderzulechner.beaconnotifier.model.BeaconModelList;


import java.util.ArrayList;
import java.util.List;

public class MyApplication extends Application {

    private BeaconManager beaconManager;
    private String scanId;
    private BeaconModelList beaconList = new BeaconModelList();


    @Override
    public void onCreate() {
        super.onCreate();
        // Initializes Estimote SDK with your App ID and App Token from Estimote Cloud.
        // You can find your App ID and App Token in the
        // Apps section of the Estimote Cloud (http://cloud.estimote.com).
        //EstimoteSDK.initialize(this, "estimote-1-4t1", "bfe9072e597b9aa36eaae64edbe4feab");
        // Configure verbose debug logging.
        //EstimoteSDK.enableDebugLogging(true);
        beaconManager = new BeaconManager(getApplicationContext());

        //setScanInterval for EddystonesListener
        beaconManager.setForegroundScanPeriod(2000, 10000);

        // Should be invoked in #onCreate.

        beaconManager.setEddystoneListener(new BeaconManager.EddystoneListener() {
            @Override
            public void onEddystonesFound(List<Eddystone> eddystones) {
                Log.d("Tag", "Nearby Eddystone beacons: " + eddystones);
                beaconList.syncRangeList(eddystones);

                /*for (Eddystone e : eddystones) {
                    if (e.telemetry != null) {
                        Log.d("Temp", "MAC address: " + e.macAddress
                                + ", temperature: " + e.telemetry.temperature);
                    }
                }*/
            }
        });

        // Should be invoked in #onStart.
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                scanId = beaconManager.startEddystoneScanning();
            }
        });
    }

    public ArrayList<BeaconModel> getBeaconList() {
        return beaconList.getBeaconList();
    }
}
