package com.example.alexanderzulechner.beaconnotifier.model;


import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by alexanderzulechner on 01.11.15.
 */
public class BeaconAPI {


    public static final String API_URL = "https://srsuwcbulu.localtunnel.me";

    private static BeaconAPI instance = null;
    private Map<String, String> headers = new HashMap<String, String>();
    private HttpBinService service;

    /**
     * Generic HttpBin.org Response Container
     */
    public static class HttpBinResponse {
        int id;
        String namespace;
        String instance;
        String shortText;
        String longText;
        Double minTemp;
        Double maxTemp;

    }

    /**
     * HttpBin.org service definition
     */
    public interface HttpBinService {
        @GET("/test.json")
        Call<List<HttpBinResponse>> getBeaconList();

        @GET("/test2.json")
        Call<HttpBinResponse> getBeacon(
                @Query("namespace") String namespace,
                @Query("instance") String instance);
    }

    /**
     * Private constructor
     */
    private BeaconAPI() {
        // Http interceptor to add custom headers to every request
        OkHttpClient httpClient = new OkHttpClient();
        httpClient.networkInterceptors().add(new Interceptor() {
            public Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder();

                System.out.println("Adding headers:" + headers);
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    builder.addHeader(entry.getKey(), entry.getValue());
                }

                return chain.proceed(builder.build());
            }
        });

        // Retrofit setup
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Service setup
        service = retrofit.create(HttpBinService.class);
    }

    /**
     * Get the HttpApi singleton instance
     */
    public static BeaconAPI getInstance() {

        if (instance == null) {
            instance = new BeaconAPI();
        }
        return instance;
    }

    /**
     * Get the API service to execute calls with
     */
    public HttpBinService getService() {
        return service;
    }

    /**
     * Add a header which is added to every API request
     */
    public void addHeader(String key, String value) {
        headers.put(key, value);
    }

    /**
     * Add multiple headers
     */
    public void addHeaders(Map<String, String> headers) {
        this.headers.putAll(headers);
    }

    /**
     * Remove a header
     */
    public void removeHeader(String key) {
        headers.remove(key);
    }

    /**
     * Remove all headers
     */
    public void clearHeaders() {
        headers.clear();
    }

    /**
     * Get all headers
     */
    public Map<String, String> getHeaders() {
        return headers;
    }
}
